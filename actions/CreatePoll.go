package actions

import (
	"errors"

	"github.com/gammazero/nexus/client"
	"github.com/gammazero/nexus/wamp"
	"gitlab.com/livesocket/poll-service/models"
	"gitlab.com/livesocket/service/lib"
)

// CreatePollAction Creates a Poll
//
// public.poll.create
// {channel string, name string, options map[string]string}
//
// Returns [Poll]
var CreatePollAction = lib.Action{
	Proc:    "public.poll.create",
	Handler: createPoll,
}

type createPollInput struct {
	Channel string
	Name    string
	Options map[string]string
}

func createPoll(args wamp.List, kwargs wamp.Dict, details wamp.Dict) *client.InvokeResult {
	// Get input args from call
	input, err := getCreatePollInput(kwargs)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Create Poll
	poll := &models.Poll{
		Channel: input.Channel,
		Name:    input.Name,
		Options: input.Options,
	}
	err := poll.Create()
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Return Poll
	return lib.ArgsResult(poll)
}

func getCreatePollInput(kwargs) (*createPollInput, error) {
	if kwargs["channel"] == nil {
		return nil, errors.New("Missing channel")
	}
	if kwargs["name"] == nil {
		return nil, errors.New("Missing name")
	}
	if kwargs["options"] == nil {
		return nil, errors.New("Missing options")
	}

	return &createPollInput{
		Channel: kwargs["channel"].(string),
		Name:    kwargs["name"].(string),
		Options: kwargs["options"].(map[string]string),
	}, nil
}
