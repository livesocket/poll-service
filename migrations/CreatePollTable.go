package migrations

import "github.com/jmoiron/sqlx"

func CreatePollTable(tx *sqlx.Tx) error {
	_, err := tx.Exec("CREATE TABLE `polls` (`channel` varchar(255) NOT NULL,`name` varchar(255) NOT NULL, PRIMARY KEY(`channel`,`name`))")
	if err != nil {
		return err
	}
	_, err = tx.Exec("CREATE TABLE `poll_options` (`channel` varchar(255) NOT NULL,`name` varchar(255) NOT NULL,`key` varchar(255) NOT NULL,`value` varchar(255) NOT NULL, PRIMARY KEY(`channel`,`name`,`key`))")
	if err != nil {
		return err
	}
	_, err = tx.Exec("CREATE TABLE `poll_votes` (`channel` varchar(255) NOT NULL,`name` varchar(255) NOT NULL,`username` varchar(255) NOT NULL,`key` varchar(255) NOT NULL, PRIMARY KEY(`channel`,`name`,`username`))")
	return err
}
