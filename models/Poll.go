package models

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/livesocket/service"
)

type Poll struct {
	Channel string            `db:"channel" json:"channel"`
	Name    string            `db:"name" json:"name"`
	Options map[string]string `json:"options"`
}

func FindPoll(channel string, name string) (*Poll, error) {
	type kv struct {
		Key   string `db:"key"`
		Value string `db:"value"`
	}

	kvs := []kv{}
	err := service.DB.Select(&kvs, "SELECT o.* FROM `polls` AS p INNER JOIN `poll_options` AS o ON p.channel=o.channel AND p.name=o.name WHERE p.channel=? AND p.name=?", channel, name)
	if err != nil {
		return nil, err
	}

	// If no options found
	if len(kvs) == 0 {
		return nil, nil
	}

	options := map[string]string{}
	for _, item := range kvs {
		options[item.Key] = item.Value
	}

	return &Poll{
		Channel: channel,
		Name:    name,
		Options: options,
	}, nil
}

func FindPollsForChannel(channel string) ([]string, error) {
	names := []string{}
	err := service.DB.Select(&names, "SELECT `name` FROM `polls` WHERE `channel`=?", channel)
	if err != nil {
		if strings.Contains(fmt.Sprint(err), "no rows") {
			return nil, nil
		}
		return nil, err
	}

	return names, nil
}

func (p *Poll) Create() error {
	tx, err := service.DB.BeginTxx(context.Background(), nil)
	if err != nil {
		return err
	}
	_, err = tx.NamedExec("INSERT INTO `poll` (`channel`,`name`) VALUES (:channel,:name)", p)
	if err != nil {
		return err
	}

	for key, value := range p.Options {
		_, err = tx.Exec("INSERT INTO `poll_options` (`channel`,`name`,`key`,`value`) VALUES (?,?,?,?)", p.Channel, p.Name, key, value)
		if err != nil {
			return err
		}
	}

	tx.Commit()
	return nil
}

func (p *Poll) Destroy() error {
	tx, err := service.DB.BeginTxx(context.Background(), nil)
	if err != nil {
		return err
	}

	_, err = tx.NamedExec("DELETE FROM `poll_votes` WHERE `channel`=:channel AND `name`=:name", p)
	if err != nil {
		return err
	}

	_, err = tx.NamedExec("DELETE FROM `poll_options` WHERE `channel`=:channel AND `name`=:name", p)
	if err != nil {
		return err
	}

	_, err = tx.NamedExec("DELETE FROM `polls` WHERE `channel`=:channel AND `name`=:name", p)
	if err != nil {
		return err
	}

	tx.Commit()
	return nil
}

func (p *Poll) Vote(username string, key string) error {
	_, err := service.DB.Exec("INSERT INTO `poll_votes` (`channel`,`name`,`username`,`key`) SELECT ?,?,?,? WHERE (SELECT count(*) FROM `poll_votes` WHERE `channel`=? AND `name`=? AND `username`=?) < 1", p.Channel, p.Name, username, key, p.Channel, p.Name, username)
	if err != nil {
		return err
	}
	return nil
}
