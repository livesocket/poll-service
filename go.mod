module gitlab.com/livesocket/poll-service

go 1.12

require (
	github.com/gammazero/nexus v2.1.2+incompatible
	github.com/jmoiron/sqlx v1.2.0
	gitlab.com/livesocket/service v1.2.1
)
