package main

import (
	"log"
	"os"
	"os/signal"

	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func main() {
	close, err := service.NewStandardService([]lib.Action{
		actions.GetPollAction,
		actions.CreatePollAction,
		actions.DestroyPollAction,
		actions.StartPollAction,
		actions.EndPollAction,
		actions.AbortPollAction,
		commands.PollCommand,
	}, nil, "__poll_service", migrations.CreateFighterTable, migrations.CreatePollTable)
	defer close()
	if err != nil {
		panic(err)
	}

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-service.Socket.Done():
		log.Print("Router gone, exiting")
		return
	}
}
